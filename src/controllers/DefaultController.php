<?php
/**
 * k4clientform plugin for Craft CMS 3.x
 *
 * client form creating
 *
 * @link      kreisvier.ch
 * @copyright Copyright (c) 2019 Christian Hiller
 */

namespace k4\k4clientform\controllers;

use craft\web\Response;
use k4\k4clientform\K4clientform;

use Craft;
use craft\web\Controller;
use k4\k4clientform\records\K4clientformRecord;
use Kint\Kint;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use XLSXWriter;

/**
 * Default Controller
 *
 * Generally speaking, controllers are the middlemen between the front end of
 * the CP/website and your plugin’s services. They contain action methods which
 * handle individual tasks.
 *
 * A common pattern used throughout Craft involves a controller action gathering
 * post data, saving it on a model, passing the model off to a service, and then
 * responding to the request appropriately depending on the service method’s response.
 *
 * Action methods begin with the prefix “action”, followed by a description of what
 * the method does (for example, actionSaveIngredient()).
 *
 * https://craftcms.com/docs/plugins/controllers
 *
 * @author    Christian Hiller
 * @package   K4clientform
 * @since     1.0.0
 */
class DefaultController extends Controller
{

    // Protected Properties
    // =========================================================================

    /**
     * @var    bool|array Allows anonymous access to this controller's actions.
     *         The actions must be in 'kebab-case'
     * @access protected
     */
    protected array|int|bool $allowAnonymous = [];


    /**
     * Handle a request going to our plugin's actionDoSomething URL,
     * e.g.: actions/k4clientform/default/do-something
     *
     * @return mixed
     */
    public function actionDownload()
    {

        $params = Craft::$app->request->getSegments();


            $rows = K4clientformRecord::find()
                ->where(array('formId'=>$params[2]))
                ->all();

            //Kint::dump($rows);

        $writer = new XLSXWriter();
        $header_style = array( 'font'=>'Arial','font-size'=>12,'font-style'=>'bold');
        $row_style = array( 'font'=>'Arial','font-size'=>12);

        $headers = [];
        $types = [];

        foreach($rows as $row) {

            $data = json_decode($row->getAttributes()["json"]);

            $data = (array) $data->form->data;

            forEach( $data as $dat)
            {
                forEach((array)$dat as $key => $value){
                    if (!in_array($key,$headers)){
                        $headers[] = $key;
                        $types[] = 'string';
                    }
                }
            }
        }

        $contents = [];
        foreach($rows as $row) {

            $data = $row->getAttributes();

            $content = array(
                $data["formId"],
                $data["dateCreated"],
                $data["url"]
            );

            $data = json_decode($row->getAttributes()["json"]);

            $data = (array) $data->form->data;

            // geh die gesammelten Header durch
            forEach($headers as $header)
            {
                $found = false;
                forEach( $data as $dat)
                {
                    forEach((array)$dat as $key => $value){
                        if ($key == $header){
                            $content[] = str_replace("&apos;","'",html_entity_decode(urldecode($value)));
                            $found = true;
                        }
                    }
                }
                if ($found == false) $content[] = "";
            }

            $contents[] = $content;


        }

        $headers = array_merge(['formId', 'dateCreated', 'url'],$headers);
        $types = array_merge(['string','string','string'],$types);

        $writer->writeSheetRow('Sheet1', $headers,$header_style);
        foreach($contents as $i=>$row)
        {
            $writer->writeSheetRow('Sheet1', $row,$row_style);
        }

        $content = $writer->writeToString();

        return Craft::$app->getResponse()->sendContentAsFile($content,"download.xlsx", [
            'mimeType' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        ]);

    }

}
