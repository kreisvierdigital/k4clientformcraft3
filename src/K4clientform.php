<?php
/**
 * k4clientform plugin for Craft CMS 3.x
 *
 * client form creating
 *
 * @link      kreisvier.ch
 * @copyright Copyright (c) 2019 Christian Hiller
 */

namespace k4\k4clientform;

use Exception;
use k4\k4clientform\fields\K4clientformField;
use k4\k4clientform\models\Settings;
use k4\k4clientform\twigextensions\K4clientformTwigExtension;
use k4\k4clientform\fields\K4clientformField as K4clientformFieldField;

use Craft;
use craft\base\Plugin;
use craft\services\Plugins;
use craft\events\PluginEvent;
use craft\web\UrlManager;
use craft\services\Fields;
use craft\events\RegisterComponentTypesEvent;
use craft\events\RegisterUrlRulesEvent;

use Kint\Kint;
use Twig\Error\LoaderError;
use Twig_Error_Loader;
use yii\base\Event;

/**
 * Craft plugins are very much like little applications in and of themselves. We’ve made
 * it as simple as we can, but the training wheels are off. A little prior knowledge is
 * going to be required to write a plugin.
 *
 * For the purposes of the plugin docs, we’re going to assume that you know PHP and SQL,
 * as well as some semi-advanced concepts like object-oriented programming and PHP namespaces.
 *
 * https://craftcms.com/docs/plugins/introduction
 *
 * @author    Christian Hiller
 * @package   K4clientform
 * @since     1.0.0
 *
 */
class K4clientform extends Plugin
{
    // Static Properties
    // =========================================================================

    /**
     * @var Test
     */
    public static $plugin;

    // Public Properties
    // =========================================================================

    /**
     * @var string
     */
    public string $schemaVersion = '1.0.0';

    public bool $hasCpSettings = true;

    // Public Methods
    // =========================================================================

    /**
     * Set our $plugin static property to this class so that it can be accessed via
     * K4clientform::$plugin
     *
     * Called after the plugin class is instantiated; do any one-time initialization
     * here such as hooks and events.
     *
     * If you have a '/vendor/autoload.php' file, it will be loaded for you automatically;
     * you do not need to load it in your init() method.
     *
     */
    public function init()
    {
        parent::init();
        self::$plugin = $this;


        // Do something after we're installed
        Event::on(
            Plugins::class,
            Plugins::EVENT_AFTER_INSTALL_PLUGIN,
            function (PluginEvent $event) {
                if ($event->plugin === $this) {
                    // We were just installed
                }
            }
        );

        $this->registerPermissions();

        $this->registerFields();
        $this->registerRoutes();
        $this->initTwigExtensions();

        $this->initLogging();
        $this->initTwigVariables();
        $this->initServices();
        $this->initAssets();
        $this->initCpEvents();


/**
 * Logging in Craft involves using one of the following methods:
 *
 * Craft::trace(): record a message to trace how a piece of code runs. This is mainly for development use.
 * Craft::info(): record a message that conveys some useful information.
 * Craft::warning(): record a warning message that indicates something unexpected has happened.
 * Craft::error(): record a fatal error that should be investigated as soon as possible.
 *
 * Unless `devMode` is on, only Craft::warning() & Craft::error() will log to `craft/storage/logs/web.log`
 *
 * It's recommended that you pass in the magic constant `__METHOD__` as the second parameter, which sets
 * the category to the method (prefixed with the fully qualified class name) where the constant appears.
 *
 * To enable the Yii debug toolbar, go to your user account in the AdminCP and check the
 * [] Show the debug toolbar on the front end & [] Show the debug toolbar on the Control Panel
 *
 * http://www.yiiframework.com/doc-2.0/guide-runtime-logging.html
 */
        Craft::info(
            Craft::t(
                'k4-clientform',
                '{name} plugin loaded',
                ['name' => $this->name]
            ),
            __METHOD__
        );
    }


    // Protected Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    protected function createSettingsModel(): ?\craft\base\Model
    {
        return new Settings();
    }

    /**
     * @inheritdoc
     */
    protected function settingsHtml(): string
    {
        return Craft::$app->view->renderTemplate(
            'k4-clientform/settings',
            [
                'settings' => $this->getSettings()
            ]
        );
    }

    protected function registerPermissions(){

    }

    protected function registerFields(){

        // Register our fields
        Event::on(
            Fields::class,
            Fields::EVENT_REGISTER_FIELD_TYPES,
            function (RegisterComponentTypesEvent $event) {
                $event->types[] = K4clientformFieldField::class;
            }
        );
    }

    protected function initLogging(){

    }

    protected function initTwigVariables(){

    }

    protected function initServices(){

    }

    protected function registerRoutes(){
        // Register our site routes
        Event::on(
            UrlManager::class,
            UrlManager::EVENT_REGISTER_CP_URL_RULES,
            function (RegisterUrlRulesEvent $event) {
                $event->rules['k4clientform/<directory:(xls|csv|html)>/<id:>'] = 'k4-clientform/default/download';
            }
        );

        // Register our site routes
        Event::on(
            UrlManager::class,
            UrlManager::EVENT_REGISTER_SITE_URL_RULES,
            function (RegisterUrlRulesEvent $event) {
                $event->rules['submit'] = 'k4clientform/default/submit';
            }
        );

        // Register our CP routes
        Event::on(
            UrlManager::class,
            UrlManager::EVENT_REGISTER_CP_URL_RULES,
            function (RegisterUrlRulesEvent $event) {
                $event->rules['cpActionTrigger1'] = 'k4clientform/default/do-something';
            }
        );


    }

    protected function initAssets(){

    }

    protected function initCpEvents(){

    }

    protected function initTwigExtensions()
    {
        if (Craft::$app->request->getIsSiteRequest()) {
            // Add in our Twig extension
            Craft::$app->view->registerTwigExtension(new K4clientformTwigExtension());
        }
    }


}
