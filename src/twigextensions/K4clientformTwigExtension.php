<?php
/**
 * k4clientform plugin for Craft CMS 3.x
 *
 * client form creating
 *
 * @link      kreisvier.ch
 * @copyright Copyright (c) 2019 Christian Hiller
 */

namespace k4\k4clientform\twigextensions;

use craft\mail\Message;
use k4\k4clientform\K4clientform;

use Craft;
use k4\k4clientform\records\K4clientformRecord;
use Kint\Kint;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;
use voku\helper\HtmlDomParser;
use yii\web\HttpException;

/**
 * Twig can be extended in many ways; you can add extra tags, filters, tests, operators,
 * global variables, and functions. You can even extend the parser itself with
 * node visitors.
 *
 * http://twig.sensiolabs.org/doc/advanced.html
 *
 * @author    Christian Hiller
 * @package   K4clientform
 * @since     1.0.0
 */
class K4clientformTwigExtension extends \Twig\Extension\AbstractExtension
{
    // Public Methods
    // =========================================================================

    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return 'K4clientform';
    }

    /**
     * Returns an array of Twig filters, used in Twig templates via:
     *
     *      {{ 'something' | someFilter }}
     *
     * @return array
     */
    public function getFilters()
    {
        return [
            new TwigFilter('k4ClientFormHandler', [$this, 'k4SendClientFormHandler']),
        ];
    }

    /**
     * Returns an array of Twig functions, used in Twig templates via:
     *
     *      {% set this = someFunction('something') %}
     *
     * @return array
     */
    public function getFunctions()
    {
        return [
            new TwigFunction('k4ClientFormHandler', [$this, 'k4SendClientFormHandler']),
        ];
    }

    /**
     * Our function called via Twig; it can do anything you want
     *
     * @return string
     */
    public function k4SendClientFormHandler($content = "", $submitButtonText = 'submit', $eMailSender = "", $eMailRecipient = "", $eMailTemplate = "",$eMailSubject = "", $eMailErrorMsg = "", $eMailSuccessMsg = "",$entryId = null, $datenschutzUrl)
    {
//        Kint::dump($content, $submitButtonText, $eMailSender, $eMailRecipient, $eMailTemplate,$eMailSubject, $eMailErrorMsg, $eMailSuccessMsg,$entryId);
//        die();

        // Prüfen zusätzliche Blindcopy?
        // Reine Textversion bei E-Mail sichergestellt?
        // Prüfen CSRF Token Handling...
        // Hinzufügen von Feld für Dateien


        if ($content != "") {

            $formHtml = "";
            $emailData = "";
            $formId = md5($content);
            $sent = false;
            $noError = true;

            //$pluginSettings = craft()->plugins->getPlugin('K4Clientform')->getSettings();
            $pluginSettings = K4clientform::$plugin->getSettings();
            $honeyPotField = $pluginSettings->eMailHoneyPot;


            if (filter_var($eMailSender, FILTER_VALIDATE_EMAIL) === false) {
                $eMailSender = $pluginSettings->eMailSender;
            }
            if (filter_var($eMailRecipient, FILTER_VALIDATE_EMAIL) === false) {
                $eMailRecipient = $pluginSettings->eMailRecipient;
            }

            if ($eMailSubject == "") {
                $eMailSubject = $pluginSettings->eMailSubject;
            }

            if ($eMailTemplate == ""){
                $eMailTemplate = $pluginSettings->eMailTemplate;
            }

            if ($eMailErrorMsg == "") {
                $eMailErrorMsg = $pluginSettings->eMailErrorMsg;
            }

            if ($eMailSuccessMsg == "") {
                $eMailSuccessMsg = $pluginSettings->eMailSuccessMsg;
            }


            if (isset($_POST[$formId])) {
                $sent = true;

                if (!$honeyPotField == "") {
                    $requestHoneyPotField = $_POST[$honeyPotField];
                    if (!$requestHoneyPotField == "") {
                        throw new HttpException(400);
                    }
                }
            }


            $formHtml = $formHtml . "<form method='post'><input type='hidden' name='" . $formId . "' value='yes'>";

            //$formHtml = $formHtml . "<input type='hidden' name='action' value='k4clientform/default/submit'>";
            $formHtml = $formHtml . "<input type='hidden' name='". Craft::$app->config->general->csrfTokenName."' value='" . Craft::$app->request->getCsrfToken()  . "'>";
            $formHtml = $formHtml . "<div style='display:none'><input type='text' name='" . $honeyPotField . "' value=''></div>";

            $html = HtmlDomParser::str_get_html($content);

            $selectedFilter = 'field';
            //set selected link
            $selectElemArr = $html->find($selectedFilter);

            $jsonElemStr = '{"formularName":"'.$eMailSubject.'"},';

            if (is_iterable($selectElemArr)) {
                // Element wurde gefunden - Active Klassen setzen

                $lastType = '';

                foreach ($selectElemArr as $element) {

                    $fieldData = "";

                    if (isset($_POST[$element->name]) && $sent) {
                        $fieldData = str_replace("\"", "&quot;", str_replace("'", "&apos;", htmlentities($_POST[$element->name])));
                    }

                    $isRequired = $element->required;
                    $isSelected = "";
                    $isRequiredAttr = "";

                    if ($isRequired) {
                        $isRequiredAttr = "required";
                    }
                    $aditionalClass = "";

                    switch ($element->type) {

                        // inputfeld
                        case "text":
                            if ($sent) {
                                if ($isRequired && $fieldData == "") {
                                    $aditionalClass = $aditionalClass . " error ";
                                    $noError = false;
                                }
                                $emailData = $emailData . "<tr><td>" . html_entity_decode($element->label) . "</td><td>" . $fieldData . "</td></tr>";
                                $jsonElemStr .= ' {"' . html_entity_decode($element->label) . '":"' . urlencode(strip_tags($fieldData)) . '"},';
                            }

                            $formHtml .= '
                                <div id="fields-' . $element->name . '-field" class="field plaintext ' . $element->name . '-field ' . $isRequiredAttr . '">
                                    <div class="heading">
                                        <label for="fields-' . $element->name . '">' . html_entity_decode($element->label) . '</label>';
                            if ($element->description == true) {
                                $formHtml .= '<small class="k4Cfrm-description">' . html_entity_decode($element->description) . '</small>';
                            }
                            $formHtml .= '
                                    </div>
                                    <div class="input">
                                        <input type="text" id="fields-' . $element->name . '" class="' . $element->name . $aditionalClass . '" 
                                        value="' . $fieldData . '" name="' . $element->name . '" 
                                        data-validation="' . $isRequiredAttr . '">
                                    </div>
                                </div>';
                            break;


                        // textarea
                        case "textarea":
                            if ($sent) {
                                if ($isRequired && $fieldData == "") {
                                    $aditionalClass = $aditionalClass . " error ";
                                    $noError = false;
                                }
                                $emailData = $emailData . "<tr><td>" . html_entity_decode($element->label) . "</td><td>" . $fieldData . "</td></tr>";
                                $jsonElemStr .= ' {"' . html_entity_decode($element->label) . '":"' . urlencode(strip_tags($fieldData)) . '"},';
                            }

                            $formHtml .= '
                                <div id="fields-' . $element->name . '-field" class="field plaintext ' . $element->name . '-field ' . $isRequiredAttr . '">
                                    <div class="heading">
                                        <label for="fields-' . $element->name . '">' . html_entity_decode($element->label) . '</label>';
                            if ($element->description == true) {
                                $formHtml .= '<small class="k4Cfrm-description">' . html_entity_decode($element->description) . '</small>';
                            }
                            $formHtml .= '
                                    </div>
                                    <div class="input">
                                        <textarea id="fields-' . $element->name . '" class="' . $element->name . $aditionalClass . '" 
                                        rows="4" 
                                        name="' . $element->name . '" 
                                        data-validation="' . $isRequiredAttr . '">' . $fieldData . '</textarea>
                                    </div>
                                </div>';
                            break;


                        // checkbox
                        case "checkbox":
                            if ($sent) {
                                if ($fieldData == "true") {
                                    $isSelected = "checked='checked'";
                                    $emailData = $emailData . "<tr><td>" . html_entity_decode($element->label) . "</td><td>isChecked</td></tr>";
                                    $jsonElemStr .= ' {"' . html_entity_decode($element->label) . '":"0"},';
                                } else {
                                    if ($isRequired) {
                                        $aditionalClass = $aditionalClass . " error ";
                                        $noError = false;
                                    }
                                    $emailData = $emailData . "<tr><td>" . html_entity_decode($element->label) . "</td><td>notChecked</td></tr>";
                                    $jsonElemStr .= ' {"' . html_entity_decode($element->label) . '":"1"},';
                                }
                            }

                            $formHtml .= '
                                <div id="fields-' . $element->name . '-field" class="field checkboxes ';
                            if ($lastType == 'checkbox') {
                                $formHtml .= ' checkboxgroup ';
                            }
                            $formHtml .= $element->name . '-field  ' . $isRequiredAttr . '">
                                 <div class="heading"></div>
                                    <div class="input">
                                        <div id="fields-' . $element->name . '" class="' . $element->name . '">
                                        <label for="fields-' . $element->name . '-1"><input type="checkbox" ' . $isRequiredAttr . ' ' . $isSelected . ' id="fields-' . $element->name . '-1" name="' . $element->name . '" value="true">&nbsp;' . html_entity_decode($element->label) . '</label>
                                        ';
                            if ($element->description == true) {
                                $formHtml .= '<small class="k4Cfrm-description">' . html_entity_decode($element->description) . '</small>';
                            }
                            $formHtml .= '</div>
				                    </div>
				                </div>';
                            break;

                        // checkbox
                        case "datenschutz":

                            $isRequiredAttr = "required";

                            if ($sent){
                                if ($fieldData == "true"){
                                    $isSelected = "checked='checked'";
                                    $emailData = $emailData.  "<tr><td>" . Craft::t('k4-clientform','k4Cfrm-datenschutz-label' ) . "</td><td>akzeptiert</td></tr>";
                                }
                                else{
                                    if ($isRequiredAttr == "required"){
                                        $aditionalClass = $aditionalClass . " error ";
                                        $noError = false;
                                    }
                                    $emailData = $emailData.  "<tr><td>" . Craft::t('k4-clientform','k4Cfrm-datenschutz-label' ) . "</td><td>nicht akzeptiert</td></tr>";
                                }
                            }
                            
                            $formHtml = $formHtml . '
                                <div id="fields-' . $element->name . '-field" class="field checkboxes ' . $element->name . '-field  ' . $isRequiredAttr . '">
                                    <div class="heading">' . Craft::t('k4-clientform','k4Cfrm-datenschutz-label' ) . '</div>
                                    <div class="input">
                                        <div id="fields-' . $element->name . '" class="' . $element->name . '">
                                        <label for="fields-' . $element->name . '-1"> <input type="checkbox" ' . $isRequiredAttr . ' ' . $isSelected . ' id="fields-' . $element->name . '-1" name="' . $element->name . '" value="true">&nbsp;' . str_replace('%%datenschutzUrl%%',$datenschutzUrl,Craft::t('k4-clientform','k4Cfrm-datenschutz-description')) . '</label> 
                                        </div>';

                            $formHtml = $formHtml . '                  
                                    </div>
                                </div>';
                            break;



                        // radiobutton
                        case "radio-group":
                            if ($sent) {
                                $selectedOption = "";
                                //remove all selected values, set selected value
                                foreach ($element->find('option') as $optionElement) {
                                    $optionElement->selected = null;
                                    if ($optionElement->value == $fieldData) {
                                        $optionElement->selected = "true";
                                        $selectedOption = $optionElement->innertext;
                                    }
                                }
                                if ($isRequired && $fieldData == "") {
                                    $aditionalClass = $aditionalClass . " error ";
                                    $noError = false;
                                }
                                $emailData = $emailData . "<tr><td>" . html_entity_decode($element->label) . "</td><td>" . $selectedOption . "</td></tr>";
                                $jsonElemStr .= ' {"' . html_entity_decode($element->label) . '":"' . urlencode(strip_tags($selectedOption)) . '"},';
                            }


                            $formHtml .= ' <div id="fields-' . $element->name . '-field" class="field radiobuttons ' . $element->name . '-field ' . $isRequiredAttr . '">
                                        <div class="heading"><span>' . html_entity_decode($element->label) . '</span>';
                            if ($element->description == true) {
                                $formHtml .= '<small class="k4Cfrm-description">' . html_entity_decode($element->description) . '</small>';
                            }
                            $formHtml .= ' 
                                        </div>
                                        <div class="input"><div id="fields-' . $element->name . '" class="' . $element->name . '">';
                            $counter = 0;
                            foreach ($element->find('option') as $optionElement) {
                                $counter++;
                                $isSelected = "";
                                if ($optionElement->selected === "true" OR $counter == 1) {
                                    $isSelected = "checked";
                                }
                                $formHtml .= ' 
                                <label for="fields-' . $element->name . '-' . $counter . '"><input type="radio" id="fields-' . $element->name . '-' . $counter . '" name="' . $element->name . '" value="' . $optionElement->value . '" ' . $isSelected . '>' . $optionElement->innertext . '</label>
                                ';
                            }
                            $formHtml = $formHtml . '</div></div></div>';
                            break;


                        // selectfeld
                        case "select":
                            if ($sent) {
                                $selectedOption = "";
                                //remove all selected values, set selected value
                                foreach ($element->find('option') as $optionElement) {
                                    $optionElement->selected = null;
                                    if ($optionElement->value == $fieldData) {
                                        $optionElement->selected = "true";
                                        $selectedOption = $optionElement->innertext;
                                    }
                                }
                                if ($isRequired && $fieldData == "") {
                                    $aditionalClass = $aditionalClass . " error ";
                                    $noError = false;
                                }
                                $emailData = $emailData . "<tr><td>" . html_entity_decode($element->label) . "</td><td>" . $selectedOption . "</td></tr>";
                                $jsonElemStr .= ' {"' . html_entity_decode($element->label) . '":"' . urlencode(strip_tags($selectedOption)) . '"},';
                            }

                            $formHtml = $formHtml . '<div id="fields-' . $element->name . '-field" class="field dropdown ' . $element->name . '-field ' . $isRequiredAttr . '">
				                        <div class="heading"><span for="fields-' . $element->name . '">' . html_entity_decode($element->label) . '</span>';
                            if ($element->description == true) {
                                $formHtml = $formHtml . '<small class="k4Cfrm-description">' . html_entity_decode($element->description) . '</small>';
                            }
                            $formHtml = $formHtml . '</div>
                                        <div class="input">
                                            <select id="fields-' . $element->name . '" class="' . $element->name . '" name="' . $element->name . '" ' . $isRequired . '>';
                            foreach ($element->find('option') as $optionElement) {
                                $isSelected = "";
                                if ($optionElement->selected === "true") {
                                    $isSelected = "selected='true'";
                                }
                                $formHtml = $formHtml . "<option name='" . $element->name . "' value='" . $optionElement->value . "' " . $isSelected . ">" . $optionElement->innertext . "</option>";
                            }
                            $formHtml = $formHtml . '</select></div></div>';
                            break;

                        // p-tag
                        case "paragraph":
                            $formHtml = $formHtml . "<p>" . html_entity_decode($element->label) . "</p>";
                            $emailData = $emailData . "<tr><td colspan='2'><p>" . html_entity_decode($element->label) . "</p></td></tr>";
                            break;

                        // h2-tag
                        case "header":
                            $formHtml = $formHtml . "<h2>" . html_entity_decode($element->label) . "</h2>";
                            if ($sent) {
                                $emailData = $emailData . "<tr><td colspan='2'><h2>" . html_entity_decode($element->label) . "</h2></td></tr>";
                            }
                            break;
                    }

                    $lastType = $element->type;
                }

                $emailData = '<table>' . $emailData . '</table>';


                $pos = strpos($eMailTemplate, "{form}");

                if ($pos === false) {
                    $eMailTemplate = $eMailTemplate. $emailData;
                } else {
                    $eMailTemplate = str_replace("{form}",$emailData, $eMailTemplate);
                    $eMailTemplate = str_replace("{subject}",$eMailSubject, $eMailTemplate);

                }

                $formHtml = $formHtml . '<div class="submit"><input type="submit" value="'.Craft::t('k4-clientform','k4Cfrm-submitButton' ).'"></div></form>';


                if ($sent && $noError) {

                    $jsonElemStr = rtrim($jsonElemStr, ",");

                    // daten in db speichern
                    $json = '{"form": {
                  "formId": "' . $entryId . '",
                    "data": [
                      ' . $jsonElemStr . '
                    ]
                }}';


                    $result = Craft::$app->db->createCommand()
                        ->insert(K4clientformRecord::tableName(), [
                            'formId' => $entryId,
                            'url' => Craft::$app->request->getUrl(),
                            'json' => $json
                        ])->execute();

                    //$email = new EmailModel();
                    $email = new Message();

                    $eMailBcc = $pluginSettings->eMailBcc;


                    $email->setTo($eMailRecipient);
                    if (!empty($eMailBcc)){
                        $email->setBcc([['email' => $eMailBcc, 'name' => $eMailBcc]]);
                    }
                    $email->setFrom($eMailSender);
                    $email->setSubject($eMailSubject);
                    $email->setHtmlBody($eMailTemplate);

                    //$this->sendMail($eMailTemplate,$eMailSubject,$eMailRecipient,$eMailBcc);
                    Craft::$app->mailer->send($email);

                    $formHtml = $eMailSuccessMsg;

                }
                if ($sent && !$noError) {
                    $formHtml = $eMailErrorMsg . $formHtml;
                }
            } else {
                $formHtml = $content . "<!-- Error, no valid form -->";
            }


            return $formHtml;
        }
    }

    /**
     * @param string                            $html
     * @param string                            $subject
     * @param array|string|\craft\elements\User $mail
     *
     * @return bool
     */
    public function sendMail(string $html, string $subject, $mailTo, $mailBcc): bool
    {
        return Craft::$app
            ->getMailer()
            ->compose()
            ->setTo($mailTo)
            ->setBcc($mailBcc)
            ->setSubject($subject)
            ->setHtmlBody($html)
            ->send();
    }

}
