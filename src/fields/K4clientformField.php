<?php
/**
 * k4clientform plugin for Craft CMS 3.x
 *
 * client form creating
 *
 * @link      kreisvier.ch
 * @copyright Copyright (c) 2019 Christian Hiller
 */

namespace k4\k4clientform\fields;

use k4\k4clientform\K4clientform;
use k4\k4clientform\assetbundles\k4clientformfieldfield\K4clientformFieldFieldAsset;

use Craft;
use craft\base\ElementInterface;
use craft\base\Field;
use craft\helpers\Db;
use k4\k4clientform\models\K4clientformModel;
use Kint\Kint;
use yii\db\Schema;
use craft\helpers\Json;

/**
 * K4clientformField Field
 *
 * Whenever someone creates a new field in Craft, they must specify what
 * type of field it is. The system comes with a handful of field types baked in,
 * and we’ve made it extremely easy for plugins to add new ones.
 *
 * https://craftcms.com/docs/plugins/field-types
 *
 * @author    Christian Hiller
 * @package   K4clientform
 * @since     1.0.0
 */
class K4clientformField extends Field
{
    // Public Properties
    // =========================================================================

    // Static Methods
    // =========================================================================

    /**
     * Returns the display name of this class.
     *
     * @return string The display name of this class.
     */
    public static function displayName(): string
    {
        return Craft::t('k4-clientform', 'K4clientformField');
    }

    // Public Methods
    // =========================================================================

    /**
     * Returns the validation rules for attributes.
     *
     * Validation rules are used by [[validate()]] to check if attribute values are valid.
     * Child classes may override this method to declare different validation rules.
     *
     * More info: http://www.yiiframework.com/doc-2.0/guide-input-validation.html
     *
     * @return array
     */
    public function rules(): array
    {
        $rules = parent::rules();
        /**
        $rules = array_merge($rules, [
            ['someAttribute', 'string'],
            ['someAttribute', 'default', 'value' => 'Some Default'],
        ]);
        */
        return $rules;
    }

    /**
     * Returns the column type that this field should get within the content table.
     *
     * This method will only be called if [[hasContentColumn()]] returns true.
     *
     * @return string The column type. [[\yii\db\QueryBuilder::getColumnType()]] will be called
     * to convert the give column type to the physical one. For example, `string` will be converted
     * as `varchar(255)` and `string(100)` becomes `varchar(100)`. `not null` will automatically be
     * appended as well.
     * @see \yii\db\QueryBuilder::getColumnType()
     */
    public function getContentColumnType(): string
    {
        return Schema::TYPE_TEXT;
    }

    /**
     * Normalizes the field’s value for use.
     *
     * This method is called when the field’s value is first accessed from the element. For example, the first time
     * `entry.myFieldHandle` is called from a template, or right before [[getInputHtml()]] is called. Whatever
     * this method returns is what `entry.myFieldHandle` will likewise return, and what [[getInputHtml()]]’s and
     * [[serializeValue()]]’s $value arguments will be set to.
     *
     * @param mixed                 $value   The raw field value
     * @param ElementInterface|null $element The element the field is associated with, if there is one
     *
     * @return mixed The prepared field value
     */
    public function normalizeValue(mixed $value, ?\craft\base\ElementInterface $element = null): mixed
    {
        if (!is_array($value)){
            return (array) json_decode($value);
        } else
            return $value;
    }

    /**
     * Modifies an element query.
     *
     * This method will be called whenever elements are being searched for that may have this field assigned to them.
     *
     * If the method returns `false`, the query will be stopped before it ever gets a chance to execute.
     *
     * @param ElementQueryInterface $query The element query
     * @param mixed                 $value The value that was set on this field’s corresponding [[ElementCriteriaModel]] param,
     *                                     if any.
     *
     * @return null|false `false` in the event that the method is sure that no elements are going to be found.
     */
    public function serializeValue(mixed $value, ?\craft\base\ElementInterface $element = null): mixed
    {
        return parent::serializeValue($value, $element);
    }

    public function getSettingsHtml(): ?string
    {
        // Render the settings template
        return Craft::$app->getView()->renderTemplate(
            'k4-clientform/_components/fields/K4clientformField_settings',
            [
                'field' => $this,
            ]
        );
    }

    public function getInputHtml(mixed $value, ?\craft\base\ElementInterface $element = null): string
    {
        if (!$value) $value = new K4clientformModel();

        $id = Craft::$app->view->formatInputId($this->handle);
        $namespacedId = Craft::$app->view->namespaceInputId($id);

        /* -- Include our Javascript & CSS */
        Craft::$app->view->registerAssetBundle(K4clientformFieldFieldAsset::class);

        /* -- Variables to pass down to our field.js */
        $jsonVars = array(
            'id' => $id,
            'name' => $this->handle,
            'namespace' => $namespacedId,
            'prefix' => Craft::$app->view->namespaceInputId('')
        );

        $jsonVars = json_encode($jsonVars);

        //var_dump($jsonVars);

        Craft::$app->view->registerJs("$('#{$namespacedId}').K4Clientform_K4ClientformFieldType(" . $jsonVars . ");");
        Craft::$app->view->registerJs("var languages = { default: {"
            . "addOption:'".$this->enhancedTranslate('k4Cfrm-addOption' )."',"
            . "allFieldsRemoved:'".$this->enhancedTranslate('k4Cfrm-allFieldsRemoved' )."',"
            . "allowSelect:'".$this->enhancedTranslate('k4Cfrm-allowSelect' )."',"
            . "autocomplete:'".$this->enhancedTranslate('k4Cfrm-autocomplete' )."',"
            . "button:'".$this->enhancedTranslate('k4Cfrm-button' )."',"
            . "cannotBeEmpty:'".$this->enhancedTranslate('k4Cfrm-cannotBeEmpty' )."',"
            . "checkboxGroup:'".$this->enhancedTranslate('k4Cfrm-checkboxGroup' )."',"
            . "checkbox:'".$this->enhancedTranslate('k4Cfrm-checkbox' )."',"
            . "datenschutz:'".$this->enhancedTranslate('k4Cfrm-datenschutz-cpInfo' )."',"
            . "checkboxes:'".$this->enhancedTranslate('k4Cfrm-checkboxes' )."',"
            . "className:'".$this->enhancedTranslate('k4Cfrm-className' )."',"
            . "clearAllMessage:'".$this->enhancedTranslate('k4Cfrm-clearAllMessage' )."',"
            . "clearAll:'".$this->enhancedTranslate('k4Cfrm-clearAll' )."',"
            . "close:'".$this->enhancedTranslate('k4Cfrm-close' )."',"
            . "content:'".$this->enhancedTranslate('k4Cfrm-content' )."',"
            . "copy:'".$this->enhancedTranslate('k4Cfrm-copy' )."',"
            . "dateField:'".$this->enhancedTranslate('k4Cfrm-dateField' )."',"
            . "description:'".$this->enhancedTranslate('k4Cfrm-description' )."',"
            . "descriptionField:'".$this->enhancedTranslate('k4Cfrm-descriptionField' )."',"
            . "devMode:'".$this->enhancedTranslate('k4Cfrm-devMode' )."',"
            . "editNames:'".$this->enhancedTranslate('k4Cfrm-editNames' )."',"
            . "editorTitle:'".$this->enhancedTranslate('k4Cfrm-editorTitle' )."',"
            . "editXML:'".$this->enhancedTranslate('k4Cfrm-editXML' )."',"
            . "fieldVars:'".$this->enhancedTranslate('k4Cfrm-fieldVars' )."',"
            . "fieldNonEditable:'".$this->enhancedTranslate('k4Cfrm-fieldNonEditable' )."',"
            . "fieldRemoveWarning:'".$this->enhancedTranslate('k4Cfrm-fieldRemoveWarning' )."',"
            . "fileUpload:'".$this->enhancedTranslate('k4Cfrm-fileUpload' )."',"
            . "formUpdated:'".$this->enhancedTranslate('k4Cfrm-formUpdated' )."',"
            . "getStarted:'".$this->enhancedTranslate('k4Cfrm-getStarted' )."',"
            . "header:'".$this->enhancedTranslate('k4Cfrm-header' )."',"
            . "hide:'".$this->enhancedTranslate('k4Cfrm-hide' )."',"
            . "hidden:'".$this->enhancedTranslate('k4Cfrm-hidden' )."',"
            . "label:'".$this->enhancedTranslate('k4Cfrm-label' )."',"
            . "labelEmpty:'".$this->enhancedTranslate('k4Cfrm-labelEmpty' )."',"
            . "limitRole:'".$this->enhancedTranslate('k4Cfrm-limitRole' )."',"
            . "mandatory:'".$this->enhancedTranslate('k4Cfrm-mandatory' )."',"
            . "maxlength:'".$this->enhancedTranslate('k4Cfrm-maxlength' )."',"
            . "minOptionMessage:'".$this->enhancedTranslate('k4Cfrm-minOptionMessage' )."',"
            . "name:'".$this->enhancedTranslate('k4Cfrm-name' )."',"
            . "no:'".$this->enhancedTranslate('k4Cfrm-no' )."',"
            . "off:'".$this->enhancedTranslate('k4Cfrm-off' )."',"
            . "on:'".$this->enhancedTranslate('k4Cfrm-on' )."',"
            . "option:'".$this->enhancedTranslate('k4Cfrm-option' )."',"
            . "optional:'".$this->enhancedTranslate('k4Cfrm-optional' )."',"
            . "optionLabelPlaceholder:'".$this->enhancedTranslate('k4Cfrm-optionLabelPlaceholder' )."',"
            . "optionValuePlaceholder:'".$this->enhancedTranslate('k4Cfrm-optionValuePlaceholder' )."',"
            . "optionEmpty:'".$this->enhancedTranslate('k4Cfrm-optionEmpty' )."',"
            . "paragraph:'".$this->enhancedTranslate('k4Cfrm-paragraph' )."',"
            . "placeholder:'".$this->enhancedTranslate('k4Cfrm-placeholder' )."',"
            . "preview:'".$this->enhancedTranslate('k4Cfrm-preview' )."',"
            . "radioGroup:'".$this->enhancedTranslate('k4Cfrm-radioGroup' )."',"
            . "radio:'".$this->enhancedTranslate('k4Cfrm-radio' )."',"
            . "removeMessage:'".$this->enhancedTranslate('k4Cfrm-removeMessage' )."',"
            . "remove:'".$this->enhancedTranslate('k4Cfrm-remove' )."',"
            . "required:'".$this->enhancedTranslate('k4Cfrm-required' )."',"
            . "richText:'".$this->enhancedTranslate('k4Cfrm-richText' )."',"
            . "roles:'".$this->enhancedTranslate('k4Cfrm-roles' )."',"
            . "save:'".$this->enhancedTranslate('k4Cfrm-save' )."',"
            . "selectOptions:'".$this->enhancedTranslate('k4Cfrm-selectOptions' )."',"
            . "select:'".$this->enhancedTranslate('k4Cfrm-select' )."',"
            . "selectColor:'".$this->enhancedTranslate('k4Cfrm-selectColor' )."',"
            . "selectionsMessage:'".$this->enhancedTranslate('k4Cfrm-selectionsMessage' )."',"
            . "style:'".$this->enhancedTranslate('k4Cfrm-style' )."',"
            . "subtype:'".$this->enhancedTranslate('k4Cfrm-subtype' )."',"
            . "text:'".$this->enhancedTranslate('k4Cfrm-text' )."',"
            . "textArea:'".$this->enhancedTranslate('k4Cfrm-textArea' )."',"
            . "toggle:'".$this->enhancedTranslate('k4Cfrm-toggle' )."',"
            . "warning:'".$this->enhancedTranslate('k4Cfrm-warning' )."',"
            . "viewXML:'".$this->enhancedTranslate('k4Cfrm-viewXML' )."',"
            . "yes:'".$this->enhancedTranslate('k4Cfrm-yes' )."'}}");
        Craft::$app->view->registerJs("jQuery(document.getElementById('". $namespacedId ."k4Cfrm-frm')).formBuilder({ messages: languages['default'] || {},         notify: {      success: function(msg) {        $('.checkbox-toggle').parent().hide();     }  }, controlOrder: ['text','textarea','select','checkbox','radio-group','header','paragraph','datenschutz'],disableFields: ['autocomplete', 'hidden','button','checkbox-group','date','file']}); jQuery('.fld-subtype [value=h1]').parent().parent().css('display','block');$('.checkbox-toggle').parent().hide();");

        /* -- Variables to pass down to our rendered template */
        return Craft::$app->view->renderTemplate(
            'k4-clientform/_components/fields/K4clientformField_custom',
            [
                'name' => $this->handle,
                'value' => $value,
                'field' => $this,
                'id' => $id,
                'namespacedId' => $namespacedId,
                'values' => $value
            ]
        );

    }

    public function enhancedTranslate($key){
        if ($key == Craft::t('k4-clientform',$key)){
            return Craft::t('k4-clientform',$key,null,null,"en");
        }
        else {
            return Craft::t('k4-clientform',$key);
        }
    }

    /**
     * Returns the input value as it should be saved to the database.
     *
     * @param mixed $value
     * @return mixed
     */
    public function prepValueFromPost($value)
    {
        return $value;
    }

    /**
     * Prepares the field's value for use.
     *
     * @param mixed $value
     * @return mixed
     */
    public function prepValue($value)
    {
        return $value;
    }

}
