<?php
/**
 * k4clientform plugin for Craft CMS 3.x
 *
 * client form creating
 *
 * @link      kreisvier.ch
 * @copyright Copyright (c) 2019 Christian Hiller
 */

namespace k4\k4clientform\models;

use craft\validators\SiteIdValidator;
use k4\k4clientform\K4clientform;

use Craft;
use craft\base\Model;

/**
 * K4clientformModel Model
 *
 * Models are containers for data. Just about every time information is passed
 * between services, controllers, and templates in Craft, it’s passed via a model.
 *
 * https://craftcms.com/docs/plugins/models
 *
 * @author    Christian Hiller
 * @package   K4clientform
 * @since     1.0.0
 */
class K4clientformModel extends Model
{

    // Public Properties
    // =========================================================================

    /**
     * @var string|null
     */
    public $k4CfrmField;

    /**
     * @var string|null
     */
    public $k4CfrmIdField;


    /**
     * @return Settings
     */
    public function init(): void
    {
        $this->k4CfrmField        = '';
        $this->k4CfrmIdField     = md5(time());

        parent::init();
    }

    // Public Methods
    // =========================================================================

    /**
     * Returns the validation rules for attributes.
     *
     * Validation rules are used by [[validate()]] to check if attribute values are valid.
     * Child classes may override this method to declare different validation rules.
     *
     * More info: http://www.yiiframework.com/doc-2.0/guide-input-validation.html
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            [['k4CfrmField','k4CfrmIdField'], 'string'],
            ['k4CfrmIdField','default','value' => md5(time())]
        ];
    }
}
