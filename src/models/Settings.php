<?php
/**
 * test plugin for Craft CMS 3.x
 *
 * test
 *
 * @link      kreisvier.ch
 * @copyright Copyright (c) 2019 Christian Hiller
 */

namespace k4\k4clientform\models;

use Craft;
use craft\base\Model;
use Kint\Kint;

/**
 * Test Settings Model
 *
 * This is a model used to define the plugin's settings.
 *
 * Models are containers for data. Just about every time information is passed
 * between services, controllers, and templates in Craft, it’s passed via a model.
 *
 * https://craftcms.com/docs/plugins/models
 *
 * @author    Christian Hiller
 * @package   Test
 * @since     1.0.0
 */
class Settings extends Model
{
    // Public Properties
    // =========================================================================

    /**
     * @var string
     */
    public $eMailSender;

    /**
     * @var string
     */
    public $eMailRecipient;

    /**
     * @var string
     */
    public $eMailBcc;

    /**
     * @var string
     */
    public $eMailSubject;

    /**
     * @var string
     */
    public $eMailTemplate;

    /**
     * @var string
     */
    public $eMailErrorMsg;

    /**
     * @var string
     */
    public $eMailSuccessMsg;

    /**
     * @var string
     */
    public $eMailHoneyPot;


    /**
     * @return Settings
     */
    public function init(): void
    {
        $this->eMailSender         = Craft::$app->getProjectConfig()->get('plugins.k4-clientform.settings.eMailSender');
        $this->eMailRecipient      = Craft::$app->getProjectConfig()->get('plugins.k4-clientform.settings.eMailRecipient');
        $this->eMailBcc            = Craft::$app->getProjectConfig()->get('plugins.k4-clientform.settings.eMailBcc');
        $this->eMailSubject        = Craft::$app->getProjectConfig()->get('plugins.k4-clientform.settings.eMailSubject');
        $this->eMailTemplate       = Craft::$app->getProjectConfig()->get('plugins.k4-clientform.settings.eMailTemplate');
        $this->eMailErrorMsg       = Craft::$app->getProjectConfig()->get('plugins.k4-clientform.settings.eMailErrorMsg');
        $this->eMailHoneyPot       = Craft::$app->getProjectConfig()->get('plugins.k4-clientform.settings.eMailHoneyPot');

        parent::init();
    }

    // Public Methods
    // =========================================================================

    /**
     * Returns the validation rules for attributes.
     *
     * Validation rules are used by [[validate()]] to check if attribute values are valid.
     * Child classes may override this method to declare different validation rules.
     *
     * More info: http://www.yiiframework.com/doc-2.0/guide-input-validation.html
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            [['eMailSender', 'eMailRecipient', 'eMailBcc', 'eMailSubject', 'eMailErrorMsg', 'eMailSuccessMsg'],'string'],
            ['eMailSubject','default','value' => 'k4 Clientform Email'],
            ['eMailErrorMsg','default','value' => 'k4 Clientform error message'],
            ['eMailHoneyPot','default','value' => 'k4-secure']
        ];
    }
}
